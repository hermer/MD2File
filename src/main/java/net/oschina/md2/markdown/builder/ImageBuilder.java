package net.oschina.md2.markdown.builder;

import net.oschina.md2.markdown.Block;
import net.oschina.md2.markdown.BlockType;
import net.oschina.md2.markdown.MDToken;
import net.oschina.md2.markdown.ValuePart;

public class ImageBuilder implements BlockBuilder{

	private String content;
	private String title;
	private String url;
//	private String alt;
	public ImageBuilder(String content){
		this.content = content;
	}
	
	public Block bulid() {
		Block block = new Block();
		block.setType(BlockType.IMG);
		block.setValueParts(new ValuePart(title), new ValuePart(url));
		return block;
	}

	public boolean isRightType() {
		int i = content.indexOf(MDToken.IMG);
		int j = content.indexOf("]", i);
		if(j>0){
			int k = content.indexOf("(", j);
			if(k>0&&k==(j+1)){
				int l = content.indexOf(")", k);
				if(l>0){
					String strHasUrl = content.substring(k+1, l).trim();
					int m = strHasUrl.indexOf(" ");
					if(m>-1){
						url = strHasUrl.substring(0, m);
					}else{
						url = strHasUrl;
					}
					title = content.substring(i + (MDToken.IMG.length()), j);
					return true;
				}
			}
		}
		return false;
	}

}
