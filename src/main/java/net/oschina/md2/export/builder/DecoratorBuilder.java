package net.oschina.md2.export.builder;

import net.oschina.md2.export.Decorator;


public interface DecoratorBuilder {
	public Decorator build();
}
