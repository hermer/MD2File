package net.oschina.md2.export.builder;

import net.oschina.md2.export.Decorator;
import net.oschina.md2.export.HTMLDecorator;

public class HTMLDecoratorBuilder implements DecoratorBuilder{

	public Decorator build() {
		return new HTMLDecorator();
	}

}
